# Sqlite

Sqlite docker image. Only contains sqlite and alpine linux (installed from apk).

## Usage

```
docker run -v $pwd:/tmp --rm jitesoft/sqlite:latest /tmp/test.db "create table tbl1(one varchar(10), two smallint)"
```

## Tags

* [`latest`](https://gitlab.com/jitesoft/dockerfiles/sqlite/blob/master/Dockerfile)
* `3.x.x` (depending on build version, check tags!)

All files can be found at [GitLab](https://gitlab.com/jitesoft/dockerfiles/sqlite)

### Image labels

This image follows the [Jitesoft image label specification 1.0.0](https://gitlab.com/snippets/1866155).

## Licenses

Check SQLITE license (Public Domain) at [SQLite webpage](https://sqlite.org/copyright.html)!
